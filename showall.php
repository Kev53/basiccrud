<?php include('partials/header.php'); ?>

<!-- Start Nabtop section -->
<?php include('partials/topnav.php'); ?>
<!-- End Nabtop section -->

<!-- Start navber section -->
<?php include('partials/navber.php'); ?>
<!-- End navber section -->

<?php include('myphp/conect.php');  ?>

<?php 
	$sql = "SELECT  * FROM myinfo";
	$result = $conn->query($sql);
?>

<div class="container">
	<div class="row">
		<div class="col-md-8">
			<div class="table-responsive">	
				<table class="table table-bordered table-hover ">
				  <thead>
				    <tr>
				      <th scope="col">Sl No</th>
				      <th scope="col">Name</th>
				      <th scope="col">Email</th>
				      <th scope="col">Action</th>
				    </tr>
				  </thead>
				  <tbody>
				    	<?php
				    	$i = 0;
							if ($result->num_rows > 0) {
								while ($row = $result->fetch_assoc()) {
						?>
							<tr>
								<td>
									<?php $i++; echo $i; ?>
								</td>
								<td>						
								<?php
									echo $row["fname"]." ".$row["lname"];
								?>
								</td>
								<td>
									<?php echo $row["email"]; ?>
								</td>
								<td>
									<a class="btn btn-sm btn-success" href="show.php?id=<?php echo $row["id"]; ?>">Show</a>
									<a class="btn btn-sm btn-info" href="edit.php?id=<?php echo $row["id"]; ?>">Edit</a>
									<a class="btn btn-sm btn-danger" href="myphp/delete.php?id=<?php echo $row["id"]; ?>">Delete</a>
								</td>
							</tr>
						<?php
								}
							}
						?>			    
				  </tbody>
				</table>
			</div>
		</div>
		<div class="col-md-4"></div>
	</div>	
</div>




<?php include('partials/footer.php'); ?>


					
				      	
				    